$LOAD_PATH << File.expand_path("../", __FILE__)

require "grpc"
require "echo_pb"
require "echo_services_pb"

stub = Echo::Echo::Stub.new("127.0.0.1:50052", :this_channel_is_insecure)
req = Echo::Request.new(message: ARGV[0] || "Hello")
p stub.echo(req)
