$LOAD_PATH << File.expand_path("../", __FILE__)

require "grpc"
require "echo_pb"
require "echo_services_pb"

class Service < Echo::Echo::Service
  def echo(request, _call)
    puts "Received: #{request.message}"
    Echo::Response.new(message: request.message)
  end
end

server = GRPC::RpcServer.new
server.add_http2_port("0.0.0.0:50052", :this_port_is_insecure)
server.handle(Service.new)
server.run_till_terminated
