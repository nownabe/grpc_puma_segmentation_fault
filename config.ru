$LOAD_PATH << File.expand_path("../", __FILE__)

require "grpc"
require "echo_pb"
require "echo_services_pb"

class App
  def initialize
    @stub = Echo::Echo::Stub.new("127.0.0.1:50052", :this_channel_is_insecure)
  end

  def call(env)
    req = Rack::Request.new(env)
    res = @stub.echo(Echo::Request.new(message: req.body.read))

    [
      200,
      { "Content-Type" => "text/plain" },
      [res.message],
    ]
  end
end

run App.new
