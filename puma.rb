threads 2, 2
workers 2
preload_app!

pidfile "puma.pid"
stdout_redirect "log/stdout.log", "log/stderr.log", true
