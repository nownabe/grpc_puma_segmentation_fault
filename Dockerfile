FROM ruby:2.5.1

COPY . /usr/src/app
WORKDIR /usr/src/app
RUN bundle install

CMD ["bundle", "exec", "puma", "-C", "puma.rb"]
