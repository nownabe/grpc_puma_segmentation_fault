# gRPC segmentation fault with puma

grpc fail with segmentation fault on cluster mode puma with preload_app.

# Dump
See `log/stderr.log`.

# Reproduce

## Docker

Run puma container.

```bash
docker run -d -p 9292:9292 -v `pwd`/log:/usr/src/app/log --name puma \
  registry.gitlab.com/nownabe/grpc_puma_segmentation_fault:master
```

Send request.

```bash
curl -X POST http://localhost:9292/ -d hello
```

Send SIGINT to container.

```bash
docker kill --signal SIGINT puma
```

Kill and remove container.

```bash
docker kill puma
docker rm puma
```

See `log/stderr.log`.


## Local

Install gems.

```bash
bundle install
```

Run puma.

```bash
bundle exec puma -C puma.rb
```

Send request.

```bash
curl -X POST http://localhost:9292/ -d hello
```

Send SIGINT to puma.

```bash
kill -SIGINT $(cat puma.pid)
```

Kill puma.

```bash
kill -SIGKILL $(cat puma.pid)
```

See `log/stderr.log`.
